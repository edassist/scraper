Code for scraping [NPTEL](https://nptel.ac.in) , [MITOCW](https://ocw.mit.edu/), and [Udacity](https://udacity.com) for course information.

For the time being only course names are extracted from MITOCW and Udacity websites.

###### NOTE
NPTEL spider doesn't work anymore because after September 2019 NPTEL changed their website's UI.