CREATE TABLE public.courses (
	title varchar(200) NOT NULL,
	department varchar(100) NOT NULL,
	course_number varchar(15) NOT NULL,
	semester varchar(30) NOT NULL,
	description text NULL,
	url varchar(400) NOT NULL,
	"level" varchar(25) NOT NULL,
	features varchar(300) NULL,
	complete_video bool NULL,
	complete_audio bool NULL,
	other_video bool NULL,
	other_audio bool NULL,
	online_textbooks bool NULL,
	complete_lectures bool NULL,
	assesments_with_solution bool NULL,
	student_projects bool NULL,
	instructor_insights bool NULL,
	instructors varchar(150) NULL,
	CONSTRAINT courses_pkey PRIMARY KEY (course_number, semester)
);
CREATE INDEX course_word_search_index ON public.courses USING gin (to_tsvector('english'::regconfig, (((((((COALESCE(title, ''::character varying))::text || ' '::text) || COALESCE(description, ''::text)) || ' '::text) || (COALESCE(department, ''::character varying))::text) || ' '::text) || (COALESCE(instructors, ''::character varying))::text)));
