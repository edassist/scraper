import psycopg2

def makeConnection():
    conn=psycopg2.connect("dbname=mitocw user=fboi")
    return conn

def instructorsPresent():
    '''Obtains the instructors already present in the database'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute('SELECT name FROM instructors;')
    instructorList=[]
    for row in cur.fetchall():
        instructorList.append(row[0])

    cur.close()
    conn.close()
    return instructorList

class coursesPipeline(object):
    def process_item(self, item, spider):
        conn=makeConnection()
        cur=conn.cursor()

        course_number=item['courseInfo'][2]
        semester=item['courseInfo'][3]
        instructorList=instructorsPresent()
        localId=len(instructorList)

        cur.execute('INSERT INTO courses VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);',item['courseInfo'])
        conn.commit()
        for instructor in item['instructors']:
            instructor=instructor.strip()
            if instructor in instructorList:
                cur.execute('INSERT INTO taught_by VALUES (%s,%s,%s);',(course_number,semester,instructorList.index(instructor)+1))
            else:
                cur.execute('INSERT INTO instructors (name) VALUES (%s);',(instructor,))
                localId+=1
                cur.execute('INSERT INTO taught_by VALUES (%s,%s,%s);',(course_number,semester,localId))
            conn.commit()

        cur.close()
        conn.close()
        return item
