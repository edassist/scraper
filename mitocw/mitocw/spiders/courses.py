# -*- coding: utf-8 -*-
from scrapy import Spider
from scrapy.selector import Selector
from selenium import webdriver
from time import sleep
from mitocw.items import coursesItem
import psycopg2

def makeConnection():
    conn=psycopg2.connect("dbname=mitocw user=fboi")
    return conn

def coursesPresent():
    '''Obtains the courses already present in the database'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute('SELECT course_number, semester FROM courses;')
    courseList=cur.fetchall()

    cur.close()
    conn.close()
    return courseList

class CoursesSpider(Spider):
    name = 'courses'
    allowed_domains = ['ocw.mit.edu']
    start_urls = ['https://ocw.mit.edu/courses/find-by-number/']
    item=coursesItem(courseInfo=[],instructors=[])

    def parse(self, response):
        courseList=coursesPresent()
        self.driver=webdriver.Firefox()
        self.driver.get(response.url)
        sleep(2)

        sel=Selector(text=self.driver.page_source)
        depts=sel.xpath('//div[@id="department"]/p')
        for i in range(35,len(depts)-1):
            element=self.driver.find_element_by_xpath('//div[@id="department"]/p['+str(i)+']')
            element.click()
            sleep(3)

            department=depts[i-1].xpath('./span/text()').get().lstrip(' - ')
            sel=Selector(text=self.driver.page_source)
            courses=sel.xpath('//ul[@class="coursesList"]/li')

            for j in range(2,len(courses)+1):
                element2=self.driver.find_element_by_xpath('//ul[@class="coursesList"]/li['+str(j)+']/ul/li/a')
                element2.click()
                sleep(2)

                sel=Selector(text=self.driver.page_source)
                self.item['courseInfo']=[]
                self.item['instructors']=[]

                description=sel.xpath('//p[@id="courseDesc"]/text()').get()
                semester=sel.xpath('//p[@id="courseSem"]/text()').get()
                url='https://ocw.mit.edu'+sel.xpath('//div[@class="images"]/a/@href').get()
                features=sel.xpath('//p[@id="courseFeatures"]/text()').get()
                course_number=courses[j-1].xpath('./ul/li/a/text()').get()
                title=courses[j-1].xpath('./ul/li[2]/h4/a/text()').get()
                level=courses[j-1].xpath('./ul/li[3]/a/text()').get()
                complete_video=courses[j-1].xpath('@data-complete_video').get()
                complete_audio=courses[j-1].xpath('@data-complete_audio').get()
                other_video=courses[j-1].xpath('@data-other_video').get()
                other_audio=courses[j-1].xpath('@data-other_audio').get()
                online_textbooks=courses[j-1].xpath('@data-online_textbooks').get()
                complete_lectures=courses[j-1].xpath('@data-complete_lectures').get()
                assesments_with_solution=courses[j-1].xpath('@data-assesments_with_solution').get()
                student_projects=courses[j-1].xpath('@data-student_projects').get()
                instructor_insights=courses[j-1].xpath('@data-instructor_insights').get()

                if (course_number,semester) in courseList:
                    continue

                instructors=sel.xpath('//p[@id="courseInstructors"]/text()').get()
                if instructors is not None:
                    self.item['instructors']=instructors.split(',')
                self.item['courseInfo']=[title,department,course_number,semester,description,url,level,features,
                                            complete_video,complete_audio,other_video,other_audio,online_textbooks,
                                            complete_lectures,assesments_with_solution,student_projects,instructor_insights]
                yield self.item
