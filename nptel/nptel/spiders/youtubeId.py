# -*- coding: utf-8 -*-
from scrapy import Spider, Request, signals
import psycopg2
import logging
from nptel.items import youtubeItem

def makeConnection():
    conn=psycopg2.connect("dbname=nptel user=fboi")
    return conn

def targetLectures():
    '''Obtains the list of lectures for which the website needs to be scraped'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute("SELECT subjectid, lecture_number FROM lectures WHERE youtube_id is NULL;")
    lectureList=cur.fetchall()

    cur.close()
    conn.close()
    return lectureList


class YoutubeIdSpider(Spider):
    name = 'youtubeId'
    allowed_domains = ['nptel.ac.in']
    start_urls = ['https://nptel.ac.in/course.php']
    item=youtubeItem(lecture=[])

    def parse(self, response):
        lectureList=targetLectures()
        for row in lectureList:
            subjectId=row[0]
            lectureNumber=row[1]
            yield Request('https://nptel.ac.in/courses/'+subjectId+'/'+str(lectureNumber) , callback=self.youtubeVideoIdExtractor,cb_kwargs=dict(subjectId=subjectId,lectureNumber=lectureNumber))

    def youtubeVideoIdExtractor(self,response,subjectId,lectureNumber):
        if ('Connection failed: Too many connections' in response.text):
            #in case when nptel denies the connection
            logging.error("Too many requests")
            yield Request(response.url , callback=self.youtubeVideoIdExtractor,cb_kwargs=dict(subjectId=subjectId,lectureNumber=lectureNumber))

        else:
            youtubeUrl=response.xpath("//embed[@type]/@src").get()
            if youtubeUrl is None:
                # the page doesn't contain a youtube video player
                logging.error('Page doesn\'t exist for '+subjectId+' '+str(lectureNumber))
                return
            self.item['lecture']=[]
            self.item['lecture']=[subjectId,lectureNumber,youtubeUrl.split('/')[4].split('?')[0]]
            yield self.item
