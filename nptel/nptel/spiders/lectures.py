# -*- coding: utf-8 -*-
from scrapy import Spider, Request, signals
import psycopg2
import logging
from nptel.items import lectureItem

def makeConnection():
    conn=psycopg2.connect("dbname=nptel user=fboi")
    return conn

def subjectsPresent():
    '''Obtains the subjects for which the website needs to be scraped'''
    conn=makeConnection()
    cur=conn.cursor()
    # cur.execute("SELECT subjectid FROM courses WHERE medium='Video' AND subjectid NOT IN (\
    #             SELECT subjectid FROM lectures\
    #             );")
    cur.execute("SELECT subjectid FROM courses WHERE state=true and medium='Video';")
    subjectList=[]
    for row in cur.fetchall():
        subjectList.append(row[0])

    cur.close()
    conn.close()
    return subjectList


class LecturesSpider(Spider):
    name = 'lectures'
    allowed_domains = ['nptel.ac.in']
    start_urls = ['https://nptel.ac.in/course.php']
    item=lectureItem(lectures=[])

    def parse(self, response):
        subjectList=subjectsPresent()
        for subject in subjectList:
            # yield Request('https://nptel.ac.in/courses/'+subject+'/' , callback=self.informationCollector,cb_kwargs=dict(subjectId=subject))
            yield Request('https://nptel.ac.in/courses/nptel_download.php?subjectid='+subject,callback=self.informationCollector,cb_kwargs=dict(subjectId=subject))

    def informationCollector(self,response,subjectId):
        if ('Connection failed: Too many connections' in response.text):
            #in case when nptel denies the connection
            logging.error("Too many requests")
            yield Request(response.url , callback=self.informationCollector,cb_kwargs=dict(subjectId=subjectId))
        else:
            #at this point of time nptel has stopped video downloads so only 3 columns are there
            numberOfColumns=3
            counter=1
            self.item['lectures']=[]

            conn=makeConnection()
            cur=conn.cursor()
            cur.execute('SELECT max(lecture_number) FROM lectures WHERE subjectid=%s;',(subjectId,))
            maxLecture_number=cur.fetchone()[0]
            # print(maxLecture_number)
            if maxLecture_number is None:
                maxLecture_number=0
            cur.close()
            conn.close()

            chk=False
            lecturesDone=[]
            # print(response.xpath('//td'))
            for tdxpath in response.xpath('//td'):
                # print(counter//3,chk)
                if counter%numberOfColumns==1 : #this gives us the lecture number
                    # print(counter)
                    lecture_number=int(tdxpath.xpath('.//text()').get())
                    if lecture_number <= maxLecture_number :
                        chk=True
                    # print(lecture_number,counter)
                if counter%numberOfColumns==2:#this gives us the title of the lecture
                    title=tdxpath.xpath('.//text()').get()
                    # print(title,counter)
                if counter%numberOfColumns==0 :#this gives us the link to download the transcipt pdf
                    if 'PDF unavailable' in tdxpath.xpath('.//text()').get():
                        pdfDownloadLink='PDF unavailable'
                    else :
                        pdfDownloadLink=tdxpath.xpath('./a/@href').get()
                        # print(pdfDownloadLink,counter)
                    if not chk:
                        # print('HEY')
                        if lecture_number not in lecturesDone:
                            self.item['lectures'].append([subjectId,title,lecture_number,pdfDownloadLink])
                            lecturesDone.append(lecture_number)
                    else :
                        chk=False
                counter+=1

            yield self.item
