# -*- coding: utf-8 -*-
import scrapy
import psycopg2
import logging
from nptel.items import coursesItem
from selenium import webdriver
from time import sleep

def makeConnection():
    conn=psycopg2.connect("dbname=nptel user=fboi")
    return conn

def subjectsPresent():
    '''Obtains the subjects already present in the database'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute('SELECT subjectid FROM courses;')
    subjectList=[]
    for row in cur.fetchall():
        subjectList.append(row[0])

    cur.close()
    conn.close()
    return subjectList

class CourseSpider(scrapy.Spider):
    name = 'course'
    allowed_domains = ['nptel.ac.in']
    start_urls = ['https://nptel.ac.in/course.php']
    item=coursesItem(databaseEntry=[])
    counter=1

    def parse(self, response):
        #selenium stuff
        self.driver = webdriver.Firefox()
        self.driver.get(response.url)
        subjectList=subjectsPresent()

        while True:
            sleep(3)
            sel=scrapy.selector.Selector(text=self.driver.page_source)
            courses=sel.xpath('//div[@class="col-md-4 individualcourse"]')
            # print(courses)
            for course in courses:
                # print(course)
                subjectId=course.xpath('.//div[@class="csname"]/i/a/@href').get().split('/')[2]
                if subjectId in subjectList:
                    continue
                logging.info("Found "+subjectId)

                disciplinename=courses.xpath('.//div[@class="csdisp"]/text()').get().split(',')[0]
                institute=courses.xpath('.//div[@class="csdisp"]/text()').get().split(',')[1]
                subjectname=course.xpath('.//div[@class="csname"]/i/a/text()').get()
                coordinator=course.xpath('.//div[@class="csinstructor"]/text()').get().split(',')[0].rstrip()
                if 'video' in course.xpath('.//div[@class="cstype"]/img/@src').get():
                    medium='Video'
                else :
                    medium='Web'
                self.item['databaseEntry']=[coordinator,disciplinename,subjectId,subjectname,institute,medium,False]

                yield self.item

            #move on to the next page
            self.counter+=1
            try:
                next_page=self.driver.find_element_by_xpath('//ul[@class="pagination"]/li[@value='+str(self.counter)+']')
            except :
                break
            try :
                next_page.click()
            except:
                element=self.driver.find_element_by_xpath('//div[@value="next"]')
                element.click()
                next_page.click()

        self.driver.quit()
