# -*- coding: utf-8 -*-
from scrapy import Spider, Request
from nptel.items import extraDownloadItem
import psycopg2
import logging

def makeConnection():
    conn=psycopg2.connect("dbname=nptel user=fboi")
    return conn

def subjectsPresent():
    '''Obtains the courses for which requests need to be issued'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute('SELECT subjectid FROM courses where medium=\'Video\'and state=True;')
    subjectList=[]
    for row in cur.fetchall():
        subjectList.append(row[0])

    cur.execute('DELETE FROM extra_downloads WHERE subjectid in (SELECT subjectid FROM courses where state=True and extra_downloads=True and medium='Video')')
    conn.commit()

    cur.close()
    conn.close()
    return subjectList

class ExtradownloadsSpider(Spider):
    name = 'extraDownloads'
    allowed_domains = ['nptel.ac.in']
    start_urls = ['https://nptel.ac.in/']
    item=extraDownloadItem(entry=[])

    def parse(self, response):
        subjectList=subjectsPresent()
        for subjectId in subjectList:
            yield Request('https://nptel.ac.in/downloads/'+subjectId+'/',callback=self.informationCollector,cb_kwargs=dict(subjectId=subjectId))

    def informationCollector(self,response,subjectId):
        '''Collects the information from the page'''
        self.item['entry']=[]
        tabs=response.xpath('//ul[@class="nav nav-tabs content-tabs tabs"]/li/a/@href').getall()
        for tab in tabs:
            counter=1
            a=len(response.xpath('//div[@id="'+tab.strip('#')+'"]/table/tbody//td'))
            b=len(response.xpath('//div[@id="'+tab.strip('#')+'"]/table/tbody/tr'))
            if b == 0:
                continue

            if a/b==4:
                for tdxpath in response.xpath('//div[@id="'+tab.strip('#')+'"]/table/tbody//td'):
                    if counter%4==1:
                        moduleName=tdxpath.xpath('.//text()').get()
                    if counter%4==2:
                        downloadLink='https://nptel.ac.in'+tdxpath.xpath('.//a/@href').get()
                    if counter%4==3:
                        description=tdxpath.xpath('.//text()').get()
                        self.item['entry'].append([subjectId,downloadLink,moduleName,description])

                    counter+=1
            elif a/b==2:
                for tdxpath in response.xpath('//div[@id="'+tab.strip('#')+'"]/table/tbody//td'):
                    if counter%2==1:
                        moduleName=tdxpath.xpath('.//text()').get()
                    if counter%2==0:
                        downloadLink='https://nptel.ac.in'+tdxpath.xpath('.//a/@href').get()
                        self.item['entry'].append([subjectId,downloadLink,moduleName,'NULL'])

                    counter+=1
            else:
                logging.error('THREE COLUMNS DETECTED')
        yield self.item
