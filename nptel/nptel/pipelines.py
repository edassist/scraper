# -*- coding: utf-8 -*-
from nptel.items import lectureItem
import psycopg2
import logging
from csv import DictReader

def makeConnection():
    conn=psycopg2.connect("dbname=nptel user=fboi")
    return conn

def databaseChangerLectures(entries):
    '''Changes database table entries if required'''
    conn=makeConnection()
    cur=conn.cursor()
    logging.info("Inserting for "+entries[0][0])
    for entry in entries:
        cur.execute("INSERT INTO lectures (subjectid,title,lecture_number,pdf_download) VALUES (%s,%s,%s,%s)",entry)

    conn.commit()
    cur.close()
    conn.close()
    return

def subjectsPresent():
    '''Obtains the subjects already present in the database'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute('SELECT subjectid FROM courses;')
    subjectList=[]
    for row in cur.fetchall():
        subjectList.append(row[0])

    cur.close()
    conn.close()
    return subjectList

def newCourses():
    csvfile=open('TentativeCourseList.csv')
    reader=DictReader(csvfile)
    newCourseList=[]
    for row in reader:
        if 'new' in row['Type'].lower():
            if row['NPTEL URL'] != '':
                newCourseList.append(row['NPTEL URL'].split('/')[4])
    return newCourseList

def coordinatorsPresent():
    '''Obtains the coordinators already present in the database'''
    conn=makeConnection()
    cur=conn.cursor()
    cur.execute('SELECT name FROM coordinators;')
    coordinatorList=[]
    for row in cur.fetchall():
        coordinatorList.append(row[0])

    cur.close()
    conn.close()
    return coordinatorList

def databaseChangerCourses(nptel):
    '''Makes changes to database if needed'''
    #this need change only on instructor is put into the table even if there are 2 or more
    subjectList=subjectsPresent()
    coordinatorList=coordinatorsPresent()
    localId=len(coordinatorList) #id field in the database
    conn=makeConnection()
    cur=conn.cursor()

    logging.info("Inserting "+nptel[2])
    cur.execute("INSERT INTO courses (disciplinename,subjectid,subjectname,institute,medium,state) VALUES (%s,%s,%s,%s,%s,%s)",nptel[1:])
    coordinator=nptel[0]
    if coordinator not in coordinatorList:
        cur.execute("INSERT INTO coordinators (name) VALUES (%s)",(coordinator,))
        conn.commit()
        coordinatorList.append(coordinator)
        localId+=1
        conn.commit()
        cur.execute("INSERT INTO taughtby (subjectid,id) VALUES (%s,%s)",(nptel[2],localId))
    else :
        cur.execute("INSERT INTO taughtby (subjectid,id) VALUES (%s,%s)",(nptel[2],coordinatorList.index(coordinator)+1))

    conn.commit()
    cur.close()
    conn.close()
    return

class coursesPipeline(object):
    def process_item(self, item, spider):
        databaseChangerCourses(item['databaseEntry'])
        return item

    def close_spider(self,spider):
        '''Sets state to True for ongoing courses'''
        conn=makeConnection()
        cur=conn.cursor()
        newCourseList=newCourses()
        cur.execute('UPDATE courses SET state = False;')#for the lectures that were added the last semester
        conn.commit()

        for subjectId in newCourseList:
            cur.execute('UPDATE courses SET state = True WHERE subjectid = %s;',(subjectId,))

        conn.commit()
        cur.close()
        conn.close()
        return

class lecturesPipeline(object):
    def process_item(self,item,spider):
        if len(item['lectures']) == 0:
            return item
        databaseChangerLectures(item['lectures'])
        return item

class youtubeIdPipeline(object):
    def process_item(self,item,spider):
        conn=makeConnection()
        cur=conn.cursor()

        cur.execute('UPDATE lectures SET youtube_id = %s WHERE subjectid=%s AND lecture_number=%s',(item['lecture'][2],item['lecture'][0],item['lecture'][1]))
        conn.commit()

        cur.close()
        conn.close()
        return item

class extraDownloadsPipeline(object):
    def process_item(self,item,spider):
        conn=makeConnection()
        cur=conn.cursor()

        if item['entry']!=[]:
            cur.execute('UPDATE courses SET extra_downloads=True WHERE subjectid=%s',(item['entry'][0][0],))
        for entry in item['entry']:
            if (entry[-1]=='NULL'):
                cur.execute('INSERT INTO extra_downloads (subjectid,download_link,module_name) VALUES (%s,%s,%s)',entry[:3])
            else :
                cur.execute('INSERT INTO extra_downloads (subjectid,download_link,module_name,description) VALUES (%s,%s,%s,%s)',entry)
        conn.commit()

        cur.close()
        conn.close()
        return item

    def close_spider(self,spider):
        conn=makeConnection()
        cur=conn.cursor()

        cur.execute('UPDATE courses SET extra_downloads=False WHERE medium=\'Video\' and extra_downloads is NULL;')
        conn.commit()

        cur.close()
        conn.close()
        return
