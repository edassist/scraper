# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class coursesItem(scrapy.Item):
    databaseEntry=scrapy.Field()

class lectureItem(scrapy.Item):
    lectures=scrapy.Field()

class youtubeItem(scrapy.Item):
    lecture=scrapy.Field()

class extraDownloadItem(scrapy.Item):
    entry=scrapy.Field()
