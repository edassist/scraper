CREATE TABLE public.courses (
	disciplinename varchar(50) NOT NULL,
	subjectid varchar(10) NOT NULL,
	subjectname varchar(200) NOT NULL,
	institute varchar(100) NOT NULL,
	medium varchar(20) NOT NULL,
	state bool NULL,
	extra_downloads bool NULL,
	coordinators varchar(150) NULL,
	CONSTRAINT courses_pkey PRIMARY KEY (subjectid)
);
CREATE INDEX course_word_search_index ON public.courses USING gin (to_tsvector('english'::regconfig, (((((COALESCE(subjectname, ''::character varying))::text || ' '::text) || (COALESCE(disciplinename, ''::character varying))::text) || ' '::text) || (COALESCE(coordinators, ''::character varying))::text)));

CREATE TABLE public.extra_downloads (
	subjectid varchar(10) NOT NULL,
	download_link varchar(400) NOT NULL,
	module_name varchar(100) NULL,
	description varchar(400) NULL,
	CONSTRAINT extra_downloads_subjectid_fkey FOREIGN KEY (subjectid) REFERENCES courses(subjectid)
);

CREATE TABLE public.lectures (
	subjectid varchar(10) NOT NULL,
	title varchar(300) NULL,
	youtube_id varchar(20) NULL,
	lecture_number int2 NOT NULL,
	video_download varchar(400) NULL,
	pdf_download varchar(400) NULL,
	CONSTRAINT lectures_pk PRIMARY KEY (subjectid, lecture_number),
	CONSTRAINT lectures_subjectid_fkey FOREIGN KEY (subjectid) REFERENCES courses(subjectid)
);

CREATE TABLE public.youtube_stuff (
	youtube_id varchar(20) NOT NULL,
	caption_name varchar(100) NOT NULL,
	captions text NOT NULL, --stores the subtitles
	important_words jsonb NULL, --important words with the time they first occurred
	CONSTRAINT youtube_pk PRIMARY KEY (youtube_id, caption_name)
);
