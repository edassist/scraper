import scrapy

class UdacityItem(scrapy.Item):
    course=scrapy.Field()
    skills=scrapy.Field()
