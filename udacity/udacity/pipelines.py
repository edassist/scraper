import psycopg2
import logging

def makeConnection():
    conn=psycopg2.connect("dbname=udacity user=fboi")
    return conn

class UdacityPipeline(object):
    def process_item(self, item, spider):
        conn=makeConnection()
        cur=conn.cursor()

        cur.execute('SELECT skill FROM skills;')
        skills=[]
        for row in cur.fetchall():
            skills.append(row[0])

        course_id=item['course'][0]
        skill_id=len(skills)

        cur.execute('INSERT INTO courses (title,school,paid,difficulty,url) VALUES (%s,%s,%s,%s,%s);',item['course'][1:])
        conn.commit()
        for skill in item['skills']:
            if skill in skills:
                cur.execute('INSERT INTO skills_covered VALUES (%s,%s);',(course_id,skills.index(skill)+1))
            else :
                cur.execute('INSERT INTO skills (skill) VALUES (%s);',(skill,))
                skills.append(skill)
                skill_id+=1
                cur.execute('INSERT INTO skills_covered VALUES (%s,%s);',(course_id,skill_id))
            conn.commit()

        conn.commit()
        cur.close()
        conn.close()

        return item
