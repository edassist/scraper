import scrapy
from udacity.items import UdacityItem
import psycopg2

def makeConnection():
    conn=psycopg2.connect("dbname=udacity user=fboi")
    return conn

def coursesPresent():
    conn=makeConnection()
    cur=conn.cursor()

    cur.execute('SELECT title FROM courses;')
    coursesList=[]
    for title in cur.fetchall():
        coursesList.append(title[0])

    cur.close()
    conn.close()
    return coursesList

class CoursesSpider(scrapy.Spider):
    name = 'courses'
    allowed_domains = ['udacity.com']
    start_urls = ['https://www.udacity.com/courses/all']
    item=UdacityItem(course=[],skills=[])

    def parse(self, response):
        coursesList=coursesPresent()
        id=len(coursesList)

        cards=response.xpath('//div[@class="card__inner card mb-0"]')
        for card in cards:
            self.item['course']=[]
            self.item['skills']=[]
            title=card.xpath('.//a[@class="capitalize"]/text()').get()
            url='https://www.udacity.com'+card.xpath('.//a[@class="capitalize"]/@href').get()
            school=card.xpath('.//div[@class="category-wrapper"]/h4/text()').get()
            if school is not None:
                school=school.strip()
            paid=card.xpath('.//span[@class="tag tag--free card ng-star-inserted"]').get()
            paid = True if paid is None else False
            temp=card.xpath('.//span[@class="capitalize"]/text()').get()
            if temp == 'advanced':
                difficulty=3
            elif temp == 'intermediate':
                difficulty=2
            else :
                difficulty=1
            temp=card.xpath('.//span[@class="truncate-content"]/span')
            skills=[]
            for skill in temp:
                skills.append(skill.xpath('text()').get().rstrip(', '))

            id+=1
            self.item['course']=[id,title,school,paid,difficulty,url]
            self.item['skills']=skills

            yield self.item
