
CREATE TABLE public.courses (
	id serial NOT NULL,
	title varchar(200) NOT NULL,
	school varchar(50) NULL,
	paid bool NOT NULL,
	difficulty int4 NOT NULL,
	url varchar(400) NOT NULL,
	CONSTRAINT courses_pkey PRIMARY KEY (id)
);
CREATE INDEX course_word_search_index ON public.courses USING gin (to_tsvector('english'::regconfig, (((COALESCE(title, ''::character varying))::text || ' '::text) || (COALESCE(school, ''::character varying))::text)));

CREATE TABLE public.skills (
	id serial NOT NULL,
	skill varchar(100) NOT NULL,
	CONSTRAINT skills_pkey PRIMARY KEY (id),
	CONSTRAINT skills_skill_key UNIQUE (skill)
);

CREATE TABLE public.skills_covered (
	course_id int4 NOT NULL,
	skill_id int4 NOT NULL,
	CONSTRAINT skills_covered_course_id_fkey FOREIGN KEY (course_id) REFERENCES courses(id),
	CONSTRAINT skills_covered_skill_id_fkey FOREIGN KEY (skill_id) REFERENCES skills(id)
);